import { ComponentFixture, TestBed } from '@angular/core/testing';

import { HomePageAdvertisementComponent } from './home-page-advertisement.component';

describe('HomePageAdvertisementComponent', () => {
  let component: HomePageAdvertisementComponent;
  let fixture: ComponentFixture<HomePageAdvertisementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ HomePageAdvertisementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(HomePageAdvertisementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
