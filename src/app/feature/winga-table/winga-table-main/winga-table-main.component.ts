import { Component, OnInit, AfterViewInit, ViewChild } from '@angular/core';

@Component({
  selector: 'winga-winga-table-main',
  templateUrl: './winga-table-main.component.html',
  styleUrls: ['./winga-table-main.component.scss']
})
export class WingaTableMainComponent implements OnInit, AfterViewInit {
  columnDefs = [
    { field: 'make', sortable: true, filter: true, checkboxSelection: true },
    { field: 'model', sortable: true, filter: true },
    { field: 'price', sortable: true, filter: true}
];

rowData = [
    { make: 'Toyota', model: 'Celica', price: 35000 },
    { make: 'Ford', model: 'Mondeo', price: 32000 },
    { make: 'Porsche', model: 'Boxter', price: 72000 },
    { make: 'Maruti', model: '800', price: 35000 },
    { make: 'Kia', model: 'Sonet', price: 32000 },
    { make: 'MM', model: 'XUV700', price: 72000 },
    { make: 'Nissan', model: 'Kicks', price: 35000 },
    { make: 'Renault', model: 'Duster', price: 32000 },
    { make: 'Merc', model: 'SL600', price: 72000 },
    { make: 'Lexus', model: 'L1', price: 35000 },
    { make: 'BMW', model: '5 Series', price: 32000 },
    { make: 'Ferrari', model: 'F50', price: 72000 }
];
  constructor() { }
  ngAfterViewInit() {
  }

  ngOnInit(): void {
  }

}
