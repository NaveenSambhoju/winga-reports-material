import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ChartsComponent } from './charts/charts.component';
import { RouterModule, Routes } from '@angular/router';
import { ChartsModule } from 'ng2-charts';
import { FlexLayoutModule } from '@angular/flex-layout';

const routes: Routes = [{   
  path: '',
  component: ChartsComponent
  }];


@NgModule({
  exports: [RouterModule],
  declarations: [
    ChartsComponent,
  ],
  imports: [
    CommonModule, RouterModule.forChild(routes),
    ChartsModule,
    FlexLayoutModule
  ]
})
export class WingaChartsModule { }
