import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';

const routes: Routes = [{   
    path: 'charts',
    loadChildren: () => import ("./feature/charts/charts.module").then(m => m.WingaChartsModule)
  }, {   
        path: 'table',
        loadChildren: () => import ("./feature/winga-table/winga-table.module").then(m => m.WingaTableModule)
  }, {   
        path: 'dashboard',
        loadChildren: () => import ("./feature/dashboard/dashboard.module").then(m => m.DashboardModule)
  }, {   
      path: 'global-settings',
      loadChildren: () => import ("./feature/global-settings/global-settings.module").then(m => m.GlobalSettingsModule)
  },{ 
        path: '', 
        redirectTo: '/charts',
        pathMatch: 'full'
 }];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
