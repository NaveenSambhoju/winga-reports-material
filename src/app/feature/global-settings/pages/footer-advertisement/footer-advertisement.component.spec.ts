import { ComponentFixture, TestBed } from '@angular/core/testing';

import { FooterAdvertisementComponent } from './footer-advertisement.component';

describe('FooterAdvertisementComponent', () => {
  let component: FooterAdvertisementComponent;
  let fixture: ComponentFixture<FooterAdvertisementComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ FooterAdvertisementComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(FooterAdvertisementComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
