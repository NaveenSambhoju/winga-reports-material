import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { MainComponent } from './main/main.component';
import { DashboardTileComponent } from './pages/dashboard-tile/dashboard-tile.component';

const routes: Routes = [{   
  path: '',
  component: MainComponent
  }];


@NgModule({
  declarations: [
    MainComponent,
    DashboardTileComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes)
  ]
})
export class DashboardModule { }
