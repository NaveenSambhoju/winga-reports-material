import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WingaFooterComponent } from './winga-footer.component';

describe('WingaFooterComponent', () => {
  let component: WingaFooterComponent;
  let fixture: ComponentFixture<WingaFooterComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WingaFooterComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WingaFooterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
