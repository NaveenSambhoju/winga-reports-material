import { ComponentFixture, TestBed } from '@angular/core/testing';

import { TeamEmailComponent } from './team-email.component';

describe('TeamEmailComponent', () => {
  let component: TeamEmailComponent;
  let fixture: ComponentFixture<TeamEmailComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ TeamEmailComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(TeamEmailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
