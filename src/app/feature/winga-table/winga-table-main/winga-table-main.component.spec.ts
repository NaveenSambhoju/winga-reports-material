import { ComponentFixture, TestBed } from '@angular/core/testing';

import { WingaTableMainComponent } from './winga-table-main.component';

describe('WingaTableMainComponent', () => {
  let component: WingaTableMainComponent;
  let fixture: ComponentFixture<WingaTableMainComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ WingaTableMainComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(WingaTableMainComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
