import { ComponentFixture, TestBed } from '@angular/core/testing';

import { GameSessionSchedulerComponent } from './game-session-scheduler.component';

describe('GameSessionSchedulerComponent', () => {
  let component: GameSessionSchedulerComponent;
  let fixture: ComponentFixture<GameSessionSchedulerComponent>;

  beforeEach(async () => {
    await TestBed.configureTestingModule({
      declarations: [ GameSessionSchedulerComponent ]
    })
    .compileComponents();
  });

  beforeEach(() => {
    fixture = TestBed.createComponent(GameSessionSchedulerComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
