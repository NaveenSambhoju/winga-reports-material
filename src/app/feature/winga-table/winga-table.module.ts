import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { AgGridModule } from 'ag-grid-angular';
import { WingaTableMainComponent } from './winga-table-main/winga-table-main.component';

const routes: Routes = [{   
  path: '',
  component: WingaTableMainComponent
  }];

@NgModule({
  declarations: [WingaTableMainComponent],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    AgGridModule.withComponents([])
  ]
})
export class WingaTableModule { }
