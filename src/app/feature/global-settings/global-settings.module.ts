import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import {MatTabsModule} from '@angular/material/tabs';

import { MainComponent } from './main/main.component';
import { GlobalSettingComponent } from './pages/global-setting/global-setting.component';
import { GameSessionSchedulerComponent } from './pages/game-session-scheduler/game-session-scheduler.component';
import { FooterAdvertisementComponent } from './pages/footer-advertisement/footer-advertisement.component';
import { TeamEmailComponent } from './pages/team-email/team-email.component';
import { HomePageAdvertisementComponent } from './pages/home-page-advertisement/home-page-advertisement.component';


const routes: Routes = [{   
  path: '',
  component: MainComponent
  }];

@NgModule({
  declarations: [
    MainComponent,
    GlobalSettingComponent,
    GameSessionSchedulerComponent,
    FooterAdvertisementComponent,
    TeamEmailComponent,
    HomePageAdvertisementComponent
  ],
  imports: [
    CommonModule,
    RouterModule.forChild(routes),
    MatTabsModule
  ]
})
export class GlobalSettingsModule { }
