import { NgModule } from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import { AppRoutingModule } from './app-routing.module';
import { AppComponent } from './app.component';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { SidenavComponent } from './core/sidenav/sidenav.component';
import { CoreModule } from './core/core.module';
import { ChartsModule } from 'ng2-charts';
import { AppStore } from './app-store/app-store.service';

@NgModule({
  declarations: [
    AppComponent,
    SidenavComponent,
  ],
  imports: [
    BrowserModule,
    AppRoutingModule,
    BrowserAnimationsModule,
    CoreModule,
    ChartsModule
  ],
  providers: [AppStore],
  bootstrap: [AppComponent]
})
export class AppModule { }

