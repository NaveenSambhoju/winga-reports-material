import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'winga-dashboard-tile',
  templateUrl: './dashboard-tile.component.html',
  styleUrls: ['./dashboard-tile.component.scss']
})
export class DashboardTileComponent implements OnInit {

  constructor() { }

  ngOnInit(): void {
  }

}
